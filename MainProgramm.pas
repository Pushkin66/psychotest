program Test;

const 
	  MaxQuestionsNumber = 100;
	MaxAnswersCount = 10;
	NumberOfMessages = 2;
	NumberOfLevels = 10;
  QuestionsFileName = 'questions.txt';
	AnswersFileName = 'answers.txt';
	AnswersMessagesFileName = 'answersMessages.txt';
	MessagesFileName = 'messages.txt';
	LevelsFileName = 'levels.txt';
	LevelsMessagesFileName = 'levelsMessages.txt';
	
var
  questions: array[1..MaxQuestionsNumber] of String;
  number: Integer;
  i: Integer;
  answersCount: Integer;
  resultsCounts: array[1..MaxAnswersCount] of Integer;
  answers: array[1..MaxAnswersCount, 1..MaxQuestionsNumber] of Integer;
  answersMessages: array[1..MaxAnswersCount] of String;
  answersCounts: array[1..MaxAnswersCount] of Integer;
  symbol: Char;
  messages: array[1..NumberOfMessages] of String;
  levelMins: array[1..NumberOfLevels] of Integer;
  levelMaxs: array[1..NumberOfLevels] of Integer;
  levelMessages: array[1..NumberOfLevels] of String;
  levelsCount: Integer;

procedure ReadQuestionsFromFile;
var
	textFile: Text;
	i: Integer;
begin
	Assign(textFile, QuestionsFileName);
	Reset(textFile);
	i := 0;
	
	while not Eof(textFile) do
	begin
		i := i + 1;
		ReadLn(textFile, questions[i]);
	end;
	
	number := i;
	Close(textFile);
end;

procedure ReadAnswersFromFile;
var
	textFile: Text;
	i: Integer;
	j: Integer;
begin
	Assign(textFile, AnswersFileName);
	Reset(textFile);
	i := 0;
	
	while not Eof(textFile) do
	begin
		i := i + 1;
		ReadLn(textFile, answersCounts[i]);
		
		for j := 1 to answersCounts[i] do
		begin
			Read(textFile, answers[i][j]);
		end;
	end;
	
	answersCount := i;	
	Close(textFile);
end;

procedure ReadMessagesFromFile;
var
	i: Integer;
	textFile: Text;
begin
	Assign(textFile, MessagesFileName);
	Reset(textFile);
	
	for i := 1 to NumberOfMessages do
	begin
		ReadLn(textFile, messages[i]);
	end;
	
	Close(textFile);
end;

procedure ReadAnswersMessagesFromFile;
var
	textFile: Text;
	i: Integer;
begin
	Assign(textFile, AnswersMessagesFileName);
	Reset(textFile);
	i := 0;
	
	while not Eof(textFile) do
	begin
		i := i + 1;
		ReadLn(textFile, answersMessages[i]);
	end;
	
	Close(textFile);
end;

procedure ReadLevelsRangeFromFile;
var
	textFile: Text;
	i: Integer;
begin
	Assign(textFile, LevelsFileName);
	Reset(textFile);
	i := 0;
	
	while not Eof(textFile) do
	begin
		i := i + 1;
		Read(textFile, levelMins[i]);
		Read(textFile, levelMaxs[i]);
	end;
	
	levelsCount := i;
	Close(textFile);
end;

procedure ReadLevelsMessagesFromFile;
var
	textFile: Text;
	i: Integer;
begin
	Assign(textFile, LevelsMessagesFileName);
	Reset(textFile);
	i := 0;
	
	while not Eof(textFile) do
	begin
		i := i + 1;
		ReadLn(textFile, levelMessages[i]);
	end;
	
	Close(textFile);
end;
	
procedure InitializeList;
begin
	ReadQuestionsFromFile;
end;

procedure InitializeCounts;
var
	i: Integer;
begin
	for i := 1 to answersCount do
	begin
		resultsCounts[i] := 0;
	end;
end;

procedure InitializeAnswers;
begin
	ReadAnswersFromFile;
	ReadAnswersMessagesFromFile;
end;

procedure InitializeMessages;
begin
  ReadMessagesFromFile;
end;

procedure InitializeLevels;
begin
  ReadLevelsRangeFromFile;
	ReadLevelsMessagesFromFile;
end;

procedure UpdateCount(index: Integer);
var
	i: Integer;
	j: Integer;
	doFound: Boolean;
begin
	doFound := FALSE;

	for i := 1 to answersCount do
	begin
		for j := 1 to answersCounts[i] do
		begin
			if answers[i][j] = index then
			begin
				doFound := TRUE;
				resultsCounts[i] := resultsCounts[i] + 1;
				break;
			end;
			
			if answers[i][i] > index then break;
		end;
		
		if doFound then break;
	end;
end;
			


procedure PrintLevel(count: Integer);
var
	i: Integer;
begin
	for i := 1 to levelsCount do
	begin
		if (count >= levelMins[i]) and (count <= levelMaxs[i]) then
		begin
			WriteLn(levelMessages[i]);
			break;
		end;
	end;
end;

procedure PrintResult;
var
	max: Integer;
	index: Integer;
	i: Integer;
begin
	max := resultsCounts[1];
	index := 1;
	
	for i := 2 to answersCount do
	begin
		if resultsCounts[i] > max then
		begin
			max := resultsCounts[i];
			index := i;
		end;
	end;
	
	WriteLn(answersMessages[index]);
	PrintLevel(max);
	
	WriteLn;
end;

procedure PrepareExecution;
begin
  InitializeList;
	InitializeAnswers;
	InitializeCounts;
	InitializeMessages;
	InitializeLevels;
end;

procedure PrintWelcomeMessage;
begin
  WriteLn(messages[1]);
end;

procedure PrintFinalMessage;
begin
  WriteLn(messages[2]);
end;
procedure ExecuteTest;
var 
  i: Integer;
begin
  for i := 1 to number do
	begin
		WriteLn(i, '. ', questions[i]);
		Read(symbol);
		ReadLn;
		
		if symbol = '=' then break;
		
		if symbol = '+' then
		begin
			UpdateCount(i);
		end
	end;
end;

begin
  PrepareExecution;
  PrintWelcomeMessage;
  ExecuteTest;	
	PrintResult;
	PrintFinalMessage;
	ReadLn;
end.
